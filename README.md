# BREAKFAST CELEBRATE

### Introduction

Breackfast Celebrate, is a small application for order registration based on details for special dates, whether they are breakfasts, chocolates, flower arrangements and others.
This information is recorded in an excel file organized by sheets where the customer profile, the administrator profile, the orders and the orders are detailed. These files are uploaded to the application through a field which, what allows is to pass this data to their respective tables in a database and at the same time being shown on the screen in the application.

# Installation

- ptyhon3 -m venv env_name
- source env_name/bin/activate
- git init (if you don't have git installed)
- git clone git@gitlab.com:Yilbert/breakfast_celebrate.git (by ssh)
- git clone https://gitlab.com/Yilbert/breakfast_celebrate.git (by http)
- cd breackfast_celebrate
- pip install -r requirements.txt
- python manage.py makemigrations
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py runserver

## MEMBERS

1. **RICARDO ENRIQUE JARAMILLO**
3
2. **YILBER MOLINA**
