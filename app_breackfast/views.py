from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from app_breackfast.get_data_breakfast import get_data_breakfast
from app_breackfast.models import Customer, Order, Owner, Product

from app_breackfast.tasks import email_confirm, get_data

# Create your views here.

def index(request):
    if request.method == 'POST':
        customer_data = request.FILES.get('customer')
        order_data = request.FILES.get('order')
        owner_data = request.FILES.get('owner')
        product_data = request.FILES.get('product')
        data = {
            'customer': customer_data,
            'order': order_data,
            'owner': owner_data,
            'product': product_data
            }
        data_br = get_data_breakfast(data)
        get_data.delay(data_br)
        data_user = [
            {'username': request.user.username,
            'name': request.user.first_name,
            'lastname': request.user.last_name,
            'email': request.user.email}
        ]
        email_confirm.delay(data_user)
        return redirect(reverse_lazy('customer'))
    return render(request, 'index.html')

def customer(request):
    customers = Customer.objects.all()
    return render(request, 'customer.html', {'customers': customers})

def order(request):
    orders = Order.objects.all()
    return render(request, 'order.html', {'orders': orders})

def product(request):
    products = Product.objects.all()
    return render(request, 'product.html', {'products': products})

def owner(request):
    owners = Owner.objects.all()
    return render(request, 'owner.html', {'owners': owners})
