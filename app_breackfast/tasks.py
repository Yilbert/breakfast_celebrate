# Create your tasks here
from datetime import datetime
import time
import pandas as pd
from django.conf import settings
from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string
from app_breackfast.models import Customer, Order, Owner, Product


@shared_task
def email_confirm(data):
    time.sleep(5)
    send_mail(
        'Confirm Registration',
        'data registered Successful',
        settings.EMAIL_HOST_USER,
        [data[0]['email']],
        fail_silently=False,
        html_message=render_to_string('email.html', {'username': data[0]['username'], 'name': data[0]['name'],
                                                     'lastname': data[0]['lastname'], 'email': data[0]['email']})
    )

@shared_task(name='data_breakfast')
def get_data(data_import):

   
    df_customer_data= data_import[0]
    df_order_data= data_import[1]
    df_owner_data= data_import[2]
    df_product_data= data_import[3]


    for customer in df_customer_data:
        Customer.objects.update_or_create(
        name=customer['name'],
        last_name=customer['last_name'],
        email=customer['email'],
        cellphone=customer['cellphone'],
        address=customer['address'],
    )
    for owner in df_owner_data:
        Owner.objects.update_or_create(
            name=owner['name'],
            last_name=owner['last_name'],
            email=owner['email'],
            cellphone=owner['cellphone'],
            address=owner['address'],
        )

    for product in df_product_data:
        Product.objects.update_or_create(
            name=product['name'],
            price=product['price'],
            observations=product['observations'],
        )

    for order in df_order_data:
        Order.objects.update_or_create(
            customer_id=Customer.objects.get(customer_id=order['customer_id']),
            owner_id=Owner.objects.get(owner_id=order['owner_id']),
            product_id=Product.objects.get(product_id=order['product_id']),
            quantity=order['quantity'],
            address_dest=order['address_dest'],
            cellphone_dest=order['cellphone_dest'],
        )