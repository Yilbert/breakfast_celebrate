from unicodedata import name
from django.db import models

# Create your models here.
class UserBase(models.Model):
    """
    User base model
    """
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    cellphone = models.CharField(max_length=20)
    address = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)

    class Meta:
        abstract = True

class Customer(UserBase):
    """
    Client model
    """
    customer_id = models.AutoField(primary_key=True)

    def __str__(self):
        return f'{self.name} {self.last_name}'

class Owner(UserBase):
    """
    Owner model
    """
    owner_id = models.AutoField(primary_key=True)

    def __str__(self):
        return f'{self.name} {self.last_name}'

class Product(models.Model):
    """
    Product model
    """
    product_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    price = models.IntegerField()
    observations = models.TextField()

    def __str__(self):
        return self.name

class Order(models.Model):
    """
    Order model
    """
    order_id = models.AutoField(primary_key=True)
    customer_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
    owner_id = models.ForeignKey(Owner, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.CharField(max_length=100)
    address_dest = models.CharField(max_length=100)
    cellphone_dest = models.CharField(max_length=20)
    
