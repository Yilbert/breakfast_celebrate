import pandas as pd
def get_data_breakfast(data_import):
    list_dict = []
    df_customer = pd.DataFrame(pd.read_excel(data_import['customer']))
    df_order = pd.DataFrame(pd.read_excel(data_import['order']))
    df_owner = pd.DataFrame(pd.read_excel(data_import['owner']))
    df_product = pd.DataFrame(pd.read_excel(data_import['product']))

    df_customer_data = df_customer.to_dict('records')
    df_order_data = df_order.to_dict('records')
    df_owner_data = df_owner.to_dict('records')
    df_product_data = df_product.to_dict('records')

    list_dict.append(df_customer_data)
    list_dict.append(df_order_data)
    list_dict.append(df_owner_data)
    list_dict.append(df_product_data)

    return list_dict