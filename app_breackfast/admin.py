from django.contrib import admin

from app_breackfast.models import Customer, Order, Owner, Product

# Register your models here.

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('customer_id', 'name', 'last_name', 'cellphone', 'address', 'email')
    search_fields = ('name', 'last_name')

@admin.register(Owner)
class OwnerAdmin(admin.ModelAdmin):
    list_display = ('owner_id', 'name', 'last_name', 'cellphone', 'address', 'email')
    search_fields = ('name', 'last_name')

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('product_id', 'name', 'price','observations')
    search_fields = ('name',)

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'customer_id', 'owner_id', 'product_id', 'quantity', 'address_dest', 'cellphone_dest')
    search_fields = ('customer_id', 'owner_id', 'product_id')
