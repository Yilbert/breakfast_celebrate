from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    path('accounts/login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('', login_required(views.index), name='index'),
    path('customer', login_required(views.customer), name='customer'),
    path('owner', login_required(views.owner), name='owner'),
    path('order', login_required(views.order), name='order'),
    path('product', login_required(views.product), name='product'),
]