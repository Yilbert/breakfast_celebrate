from django.apps import AppConfig


class AppBreackfastConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app_breackfast'
