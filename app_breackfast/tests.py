from django.test import TestCase
#import pytest
from django.urls import reverse
from django.urls import reverse_lazy
from .models import Customer
from django.contrib.auth.models import User  



class CustomerListView(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='rikaja', password='rika2022')

    def test_login(self):
        self.client.login(username='rikaja', password='rika2022')
        response = self.client.get(reverse_lazy('login'))
        self.assertEqual(response.status_code, 200)


    def test_customer_list_is_empty(self):
        self.client.login(username='rikaja', password='rika2022')
        Customer.objects.all().delete()
        response = self.client.get(reverse_lazy('customer'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['customers'], [])

    def test_create_customer(self):
        count_customer = Customer.objects.count()
        customer_name = 'yilbert'
        lastname = "Molina"
        
        customer = Customer.objects.create(name=customer_name, last_name=lastname)
        self.assertEqual(count_customer + 1, Customer.objects.count())
        self.assertEqual(str(customer), f'{customer_name} {lastname}')

    def test_customer_list_is_not_empty(self):
        self.client.login(username='rikaja', password='rika2022')
        customer_name = 'yilbert'
        lastname = 'Molina'
        Customer.objects.update_or_create(name=customer_name, last_name=lastname)
        response = self.client.get(reverse_lazy('customer'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['customers'], ['<Customer: yilbert Molina>'])




